"use strict"; // ���� �� ECMAScript 6, ����� ����� ��� 5-�� babel'�� ����������.

let config = require('./config.js');

let SteamUser = require('steam-user'); // non-instance'� ��������)
let registerClient = new SteamUser();
let faker = require('faker');
let fs = require('fs-extra');
let prompt = require('prompt');

registerClient.on('loggedOn', () => {
    console.log(`Logged on`);
    createAccount(true);
});

registerClient.on('error', (err) => {
    console.log(`Error: ${err}`);
});

registerClient.on('disconnected', (result, msg) => {
    console.log(`Disconnected: ${result}, ${msg}`);
});

registerClient.logOn(); // anonymously log on, required for registration

function createAccount(shouldGenerate, login, pass, email) {
    if(shouldGenerate) {
        login = config.prefix + faker.internet.userName();
        pass = faker.internet.password();
        email = faker.internet.email();
    }

    console.log(`Trying to create with l: ${login}, p: ${pass}, e: ${email}`);

    registerClient.createAccount(login, pass, email, (result, steamid) => {
        console.log(`Result is ${result}`);
        if(result != SteamUser.EResult.OK) {
            if(isDataIncorrect(result))
                createAccount(true);
            else
                createAccount(false, login, pass, email);

            return;
        }

        saveOrUpdateAccount({
            login: login,
            password: pass,
            email: email
        });

        console.log('Add mobile phone to this account');

        prompt.start();
        prompt.get([{
            name: 'pressed',
            description: 'Press enter when you finish',
            required: false
        }], (err, result1) => {
            if(err) {
                outputError(err);
                return;
            }

            let client = new SteamUser();

            client.on('loggedOn', () => {
                enableTwoFactor(client);
            });

            client.logOn({
                accountName: login,
                password: pass
            });
        });
    });
}

function enableTwoFactor(client) {
    client.enableTwoFactor((err, response) => {
        if(err) {
            outputError(err);
            return;
        }

        saveOrUpdateAccount({
            authenticator: response
        });

        prompt.get([{
            name: 'code',
            description: 'Enter SMS code that you received',
            required: true
        }], (err, result2) => {
            if(err) {
                outputError(err);
                return;
            }

            client.finalizeTwoFactor(response.shared_secret, result2.code, (err) => {
                if(err) {
                    outputError(err);
                    return;
                }

                console.log('Everything\'s done');
            });
        });
    });
}

function outputError(err) {
    console.log('An error has occurred:');
    console.log(err);
}

function isDataIncorrect(result) {
    return (
        result == SteamUser.EResult.DuplicateName
        || result == SteamUser.EResult.IllegalPassword
    );
}

function saveOrUpdateAccount(data) {
    let filename = 'account.json';

    if(fs.existsSync(filename)) {
        try {
            data = Object.assign(fs.readJsonSync(filename), data);
        } catch (err) {
            outputError(err);
        }
    }

    fs.writeJsonSync(filename, data);
}


